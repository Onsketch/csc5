/**************************************************************************
 * Programmed By : Andrew Lalyre
 * Class         : CSC5
 * Section       : TTH - 1800-1930
 * project 2     : Repetition & Switch Statements
 *************************************************************************/

#include <iostream>
using namespace std;
/**************************************************************************
 *
 * Compute Grade points and G.P.A
 * ________________________________________________________________________
 *
 * ________________________________________________________________________
 * INPUT
 *
 *
 * OUTPUT
 *
 *
 *************************************************************************/

bool check_4_winner();
void player_turn();
void board();
char location[3][3] = { { '1', '2', '3' }, { '4', '5', '6' }, { '7', '8', '9' } };

bool draw = false;
char turn;

int main()
{
     /**********************************************************************
     * CONSTANTS
     * --------------------------------------------------------------------
     *
     *********************************************************************/
	cout << "X starts the game" << endl;
	turn = 'X';
	while (!check_4_winner())
	{
		board();
		player_turn();
		check_4_winner();
	}
	if (turn == 'O' && !draw)
	{
		board();
		cout << endl  << "X is the winner O loses";
	}
	else if (turn == 'X' && !draw)
	{
		board();
		cout << endl  << "O is the winner X loses";
	}
	else
	{
		board();
		cout << endl << endl << "It's a draw! Game Over!\n";
	}
}

void board()
{
	
	cout << "| " << location[0][0] << " | " << location[0][1] << " | " << location[0][2] << " |\n";
	cout << "|___|___|___|\n";
	cout << "| " << location[1][0] << " | " << location[1][1] << " | " << location[1][2] << " |\n";
	cout << "|___|___|___|\n";
	cout << "| " << location[2][0] << " | " << location[2][1] << " | " << location[2][2] << " |\n";
	cout << "|___|___|___|\n";

}
void player_turn()
{
	int choice = 0;
	int row=0;
	int col=0;
	char turn = 'X';
	
	cout << "choose a block " << endl;
	
	if (turn == 'X')
	{
		cout << "X's turn" << endl;
	}
	else if (turn == 'O')
	{
		cout << "O's turn" << endl;
	}

	cin >> choice;
	switch (choice)
	{
	case 1:{row = 0; col = 0; break; }
	case 2:{row = 0; col = 1; break; }
	case 3:{row = 0; col = 2; break; }
	case 4:{row = 1; col = 0; break; }
	case 5:{row = 1; col = 1; break; }
	case 6:{row = 1; col = 2; break; }
	case 7:{row = 2; col = 0; break; }
	case 8:{row = 2; col = 1; break; }
	case 9:{row = 2; col = 2; break; }
	default:{cout << "Enter a valid number " << endl;
		player_turn(); }
	}

	if (turn == 'X' && location[row][col] != 'X'&&location[row][col]!='O')// i think the problem is somewhere around here i dont understand why it does not wsitch to O
	{
		location[row][col] = 'X';
		turn == 'O';// should switch to O but stays asX for some odd reason
	}
	else if(turn == 'O' && location[row][col] != 'X'&&location[row][col] != 'O')
	{
		location[row][col] = 'O';
		turn == 'X';
	}
	else
	{ 
		cout << "that block is already taken, and quit stalling play the game and take the lose" << endl;
		player_turn();
	}	
}
bool check_4_winner()
{
	for (int i = 0; i < 3; i++)
	{
		if ((location[i][0] == location[i][1] && location[i][1] == location[i][2]) ||
			(location[0][i] == location[1][i] && location[1][i] == location[2][1]) ||
			(location[i][i] == location[1][1] && location[1][1] == location[2][2]) ||
			(location[0][2] == location[1][1] && location[1][1] == location[2][0]))
		{
			return true;
		}
	}
	for (int y = 0; y < 3; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			if (location[y][x] != 'X' || location[y][x] != 'O')
			{
				return false;
			}
		}
	}
	draw = true;
	return true;
}