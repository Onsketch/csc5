/*******************************************************************************
 * Programmed By : Andrew Lalyre
 * Class         : CSC5
 * Section       : TTH - 1800-1930
 * Assignment #1 : Basic Input/ Output
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

/*******************************************************************************
 *
 * COMPUTE RERROACTIVE PAY
 * _____________________________________________________________________________
 * This program accepts as user input an employee
 * name, current annual salary, new monthly
 * salary and retroactive pay due. The program will
 * execute three times prompting the user for the
 * appropriate input and then displaying the computed
 * values for the given input.
 *
 * Computations are based on the assumption that input
 *  values are effective on January 1 and calculations
 * are effective for July 1
 *
 * _____________________________________________________________________________
 * INPUT
 *   nameFull        : Employee's full name
 *   salaryCurrent   : Current annual salary
 *   percent         : Percent increase due
 *
 * OUTPUT
 *   salaryNew       : New salary after applying rate increase
 *   salaryMonthly   : New monthly salary
 *   retroactivePay  : Retroactive pay due employee
 *
 ******************************************************************************/
int main()
{
    /***************************************************************************
     * CONSTANTS
     * -------------------------------------------------------------------------
     * MONTHS        : Total number of months
     * RETRO_MONTHS  : Number of months retroactive
     **************************************************************************/
    const int MONTHS = 12;

    string nameFull;         //INPUT - Employee's Last name
    float salaryCurrent;     // INPUT - Current annual salary
    float percentIncrease;   // INPUT - Percent increase due
    float salaryNew;         // OUTPUT - New salary after increase
    float salaryMonthly;     // OUTPUT - New monthly salary
    float retroactivePay;    // OUTPUT - Retroactive pay due employee
    const char PROGRAMMER[30] = "Andrew Lalyre";
    const char CLASS[5] = "CSC5";
    const char SECTION[25] ="TTH 1800 - 2110";
    const int  LAB_NUM = 1;
    const char LAB_NAME[17] = "01";

		// lines 64 - 71 are used as an identifier of Author to this class project program
         cout << left;
		 cout << "***********************************************************";
		 cout << "\n* PROGRAMMED BY : " << PROGRAMMER;
		 cout << "\n* " << setw(14) << "CLASS" << ": " << CLASS;
		 cout << "\n* " << setw(14) << "SECTION" << ": " << SECTION;
		 cout << "\n* Project # " << setw(9) << ": " <<LAB_NAME;
		 cout << "\n***********************************************************\n\n";
		 cout << right;

    for (int i=0; i<3;i++)
    {
        // used to gather employee name
        cout << "Please enter name:                       ";
        getline(cin,nameFull);

        // used to gather current annual salary information.
        cout << "Please enter current annual salary:      ";
        cin >> salaryCurrent ;
        cin.ignore(1000,'\n');

        // used to gather the percent of increase of salary.
        cout << "Please enter percent of salary increase: ";
        cin >> percentIncrease ;
        cin.ignore(1000,'\n');


        // formula to determine new salary after increase
        salaryNew = salaryCurrent + (salaryCurrent*percentIncrease);

        // formula to determine monthly salary.
        salaryMonthly = salaryNew/MONTHS;

        // formula to determine retroactive pay.
        retroactivePay = (salaryNew-salaryCurrent)/2;


        cout << fixed << showpoint << setprecision(2);
        cout << "\n" << nameFull << "'s SALARY INFORMATION " << endl;
        cout << "New Salary" << setw(19);
        cout << "Monthly Salary" << setw(20);
        cout << "Retroactive Pay\n";
        cout << setw(10) << salaryNew;
        cout << setw(19) <<salaryMonthly;
        cout << setw(19) <<retroactivePay<< endl;
        cout << endl;
        cout << "<Press enter to continue>";
        cin.get();
        cout << endl;
        cout << endl;
        cout << endl;




    }


    return 0;
}
