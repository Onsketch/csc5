/**************************************************************************
 * Programmed By : Andrew Lalyre
 * Class         : CSC5
 * Section       : TTH - 1800-1930
 * project 2     : Repetition & Switch Statements
 *************************************************************************/

#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

/**************************************************************************
 *
 * Compute Grade points and G.P.A
 * ________________________________________________________________________
 *
 * ________________________________________________________________________
 * INPUT
 *
 *
 * OUTPUT
 *
 *
 *************************************************************************/
int main()
{
    /**********************************************************************
     * CONSTANTS
     * --------------------------------------------------------------------
     *
     *********************************************************************/

char userInput;
int count2 = 0;
int count3 = 0;
double gpa;

//	 lines 64 - 71 are used as an identifier of Author to this class
//          project program
cout << left;
cout << "*******************************************************";
cout << "\n* PROGRAMMED BY : " << "Andrew Lalyre";
cout << "\n* " << setw(14) << "CLASS" << ": " << "CSC 5";
cout << "\n* " << setw(14) << "SECTION" << ": " << "TTH 1800-2110";
cout << "\n* " << setw(14) << "Project # " << ": " <<"02";
cout << "\n*************************************************\n\n";
cout << right;

    for (int i=1; i<4;i++)
    {
      cout << "Test #" << i << ":" << endl;
      cout << endl;
	do
	{
	  cout << "	Enter Letter Grade (enter 'X' to exit):";
	  cin.get(userInput);
	  cin.ignore(1000,'\n');

             while (userInput!='A' && userInput!='a' && userInput!='B' &&
		    userInput!='b' && userInput!='C' && userInput!='c' &&
		    userInput!='D' && userInput!='d' && userInput!='F' &&
		    userInput!='f' && userInput !='X' && userInput !='x')
	     {
		cout << "\n	Invalid letter grade, please try again.\n";
		cout << "\n	Enter Letter Grade (enter 'X' to exit):";
		cin >> userInput;
	     }

          switch(userInput)
	  {
	    case 'A':
	    case 'a': count2+=4;
	               count3++;
	    break;
	    case 'B':
	    case 'b': count2+=3;
	              count3++;
	    break;
	    case 'C':
	    case 'c': count2+=2;
	              count3++;
	    break;
	    case 'D':
	    case 'd': count2+=1;
                      count3++;
	    break;
	    case 'F':
	    case 'f': count2+=0.0;
                      count3++;
	    break;
	    default:
	            cout << "";

	  }
	  
          cout << fixed << showpoint << setprecision(2);
	  gpa = count2/count3;

        } while(userInput !='X' && userInput!='x');

      cout << endl;
      cout << "Total Grade Points: " << count2 << endl;
      cout << "GPA: " << gpa << endl;
      cout << endl;
      cout << endl;
    }

   return 0;
}
