/**************************************************************************
 * Programmed By : Andrew Lalyre
 * Class         : CSC5
 * Section       : TTH - 1800-1930
 * project 2     : Repetition & Switch Statements
 *************************************************************************/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void GetSalesInfo(int &, int &, int &, int &, string &, double &, int &);
double CalcDiscount(int &, double &);
double CalcSalesTax(string &, double , double);
double CalcShipping(int);
void OutputInvoice(int, int, int, int, string, double, int, double, double,
				   double, double );
/**************************************************************************
 *
 * Compute Invoice using Functions
 * ________________________________________________________________________
 * This program accepts user input as amount, date, and county code.
 * the program will use various function to calculate the discount, sales
 * tax , the shipping cost and the total cost of everything.
 * ________________________________________________________________________
 * INPUT
 *	accountNum        // amount number for invoice
 *	month             // gets a month in number format
 *	day               // gets a day in number format
 *	year              // gets a year in number format
 *	countyCode        // gets the first letter of a county
 *	salesAmount       // the  amount of the invoice before discount and tax
 *	weight            // gets the weight of the order
 *
 * OUTPUT
 * 	discount          // has the discount that will be applied to the
 * 	                  // invoice
 *	salesTax          // has the sales tax that will be applied to the
 *	                  // invoice
 *	ship              // has the shipping coast that will be applied to the
 *	                  // invoice
 *	total             // will display the total invoice
 *
 **************************************************************************
 * GetSalesInfo
 *-------------------------------------------------------------------------
 * The purpose for this function is to assign values to the variables that
 * were declared in main. This is to change the information that was stored
 * in the variables so they can be modify later.
 *
 * INPUT
 *	accountNum        // amount number for invoice
 *	month             // gets a month in number format
 *	day               // gets a day in number format
 *	year              // gets a year in number format
 *	countyCode        // gets the first letter of a county
 *	salesAmount       // the  amount of the invoice before discount and tax
 *	weight            // gets the weight of the order
 *
 *	Output
 *	accountNum        // amount number for invoice
 *	month             // gets a month in number format
 *	day               // gets a day in number format
 *	year              // gets a year in number format
 *	countyCode        // gets the first letter of a county
 *	salesAmount       // the  amount of the invoice before discount and tax
 *	weight            // gets the weight of the order
 **************************************************************************
 *
 **************************************************************************
 * CalcDiscount
 * ------------------------------------------------------------------------
 * The purpose of this function is to get the discount for the invoice.
 * The way this is being done is by getting the month that was entered in
 * function GetSalesInfo and checking it through out certain conditions.
 * Once the right one has been found  the salesAmount will be multipyed
 * by the approtie number and then giving to a the variable discount
 *
 * Input
 * 	JanMay            // holds the value of 0.05
 *	JunAug            // holds the value of 0.10
 *	SepDec            // holds the value of 0.15
 *
 * Output
 *	discount          // holds the discount amount for the invoice
 *************************************************************************
 *
 **************************************************************************
 * CalcSalesTax
 * -----------------------------------------------------------------------
 * The purpose for this function is to calcute the sales tax for this
 * invoice the way this is being done is by passing the information
 * country code, sales amount from GetSaleinfo function and discount from
 * the CalcDiscount. the variable countryCode will be checked against
 * certain conditions. Once a condition has been meet the a certain
 * variable will be multiplied by the subtraction of salesAmount and
 * discount.
 *
 * Input
 * 	orangeCountyTax     // holds the value 0.0775
 *  SanDiegoCountyTax   // holds the value 0.0825
 *  LosAngelesCountyTax // holds the value 0.0800
 *
 * OutPut
 * 	salesTax            // holds the sales Tax for the invoice
 *************************************************************************
 *
 **************************************************************************
 * CalcShipping
 * ------------------------------------------------------------------------
 * the purpose for this function is to calculate the shipping cost for
 * the invoice. The way in which it does is by passing weight from the
 * GetSalesInfo function and checking it against certain conditions and
 * adding 5 to the value of the variable and if the value of weight is
 * over 25 it will need to be subtracted and multiplied by a value.
 *
 * Input
 * 	ship              // holds the value of 5
 *
 * Output
 *  ship              // holds the shipping cost of the invoice
 *
 *************************************************************************
 *
 **************************************************************************
 * OutputInvoice
 * -----------------------------------------------------------------------
 * The purpose of this function is to display the invoice in to a neat
 * style. The account number and the county will be displayed on one line
 * and the rest of the invoice information will be displayed like a receipt
 * you would receive at a store were everything is easily displayed and easy
 * to find.
 ************************************************************************
 *
 *************************************************************************/

int main()
{
    /**********************************************************************
     * CONSTANTS
     * --------------------------------------------------------------------
     *
     *********************************************************************/


// lines 58 - 65 are used as an identifier of Author to this class
//  project program.
	cout << left;
	cout << "*******************************************************";
	cout << "\n* PROGRAMMED BY : " << "Andrew Lalyre";
	cout << "\n* " << setw(14) << "CLASS" << ": " << "CSC5";
	cout << "\n* " << setw(14) << "SECTION" << ": " << "TTH 1800 - 2110";
	cout << "\n* Project # " << "    : " << "02";
	cout << "\n***************************************************\n\n";
	cout << right;

	for (int i = 0 ; i < 3 ; i++ )
	{
		int accountNum;     // Input - amount number for invoice
		int month;          // Input - gets a month in number format
		int day;            // Input - gets a day in number format
		int year;           // Input - gets a year in number format
		string countyCode;  // Input - gets the first letter of a county
		double salesAmount; // Input - the  amount of the invoice before
		                    //         discount and tax
		int weight;         // Input - gets the weight of the order
		double discount;    // Output - has the discount that will be
		                    //          applied to the invoice
		double salesTax;    // Output - has the discount that will be
                            //          applied to the invoice
		double ship;        // Output - has the discount that will be
                            //          applied to the invoice
		double total;       // Output - has the discount that will be
                            //          applied to the invoice
		// line 86 is a function that will give values to the
        // accountNum,month,day ,year, countyCode,salesAmount,weight
		GetSalesInfo(accountNum,month,day ,year,
				countyCode,salesAmount,weight);
		// line 90 is a function that will calculate a discount and apply
		// it to a variable called discount.
		discount = CalcDiscount(month, salesAmount);
		// line 93 is a function that will calculate the sales tax and
		// Assign it to a variable called salesTax
		salesTax = CalcSalesTax(countyCode,salesAmount,discount);
		// line 96 is a function that will calculate the shipping cost
		// and assign the value to a variable called ship
		ship = CalcShipping(weight);
		// line 99 is math problem that will calculate the entire cost for
		// the invoice
		total = salesAmount-discount+salesTax+ship;
		// line 103 is a function that will display all the information
		// gathered from the other programs.
		OutputInvoice(accountNum, month, day , year,
				      countyCode, salesAmount,weight, discount,salesTax,
				      ship,total);
	}
   return 0;
}
 /*************************************************************************
  * GetSalesInfo
  * This function has user input the following data:
  * account number, date of sale, the county code, total
  * sale amount before tax, and the shipping weight.
  ************************************************************************/
void GetSalesInfo(int &accountNum, int &month, int &day , int &year,
		string &countyCode, double &salesAmount, int &weight)
{

// lines 211 -225 get user input.
 cout << "Please Enter your Account Number: ";
 cin >> accountNum;
 cout << "Please Enter the Sales Date." << endl;
 cout << "Enter Month (MM format): ";
 cin >> month;
 cout << "Enter Day (DD format): ";
 cin >> day;
 cout << "Enter Year (YYYY format): ";
 cin >> year;
 cout << "Please Enter the County Code: ";
 cin >> countyCode;
 cout << "Please Enter the Sales Amount: ";
 cin >> salesAmount;
 cout << "Please Enter The Weight: ";
 cin >> weight;
}

/*************************************************************************
 * CalcDiscount
 * This function uses month and saleAmount from the
 * function GetSalesInfo to determine amount of a
 * discount the customer will receive on their purchase
 ************************************************************************/
double CalcDiscount(int &month, double &salesAmount)
{
	 double JanMay = .05;    // Input - holds the value of 0.05
	 double JunAug = .10;    // Input - holds the value of 0.10
	 double SepDec = .15;    // Input - holds the value of 0.15
	 double discount;        // Output - holds the discount amount for the
	                         //          invoice
// line 242 checks to see if the value in month falls in a range of 1 - 5
	 if (month >= 1 && month <= 5)
	 {
		 discount = (JanMay*salesAmount);
	 }
// line 247 checks to see if the value in month falls in a range of 6 - 8
	 else if (month >=6 && month <= 8)
	 {
		 discount = (JunAug*salesAmount);
	 }
// line 252 checks to see if the value in month falls in a range of 9 - 12
	 else if (month >= 9 && month <= 12)
	 {
		 discount = (SepDec*salesAmount);
	 }
// line 257 tell the user that they have made an invalid choice
	 else
	 {
		 cout << "You have reached a no no place";
	 }
	 return discount;
}

/*************************************************************************
 * CalcSalesTax
 * This function will use countyCode , the samesAmount and the result
 * from function CalcDiscount named discount to determine the sales tax
 ************************************************************************/
double CalcSalesTax(string &countyCode, double salesAmount, double discount)
{
	 double orangeCountyTax = 0.0775;
	 double SanDiegoCountyTax = 0.0825;
	 double LosAngelesCountyTax = 0.0800;
	 double salesTax;

// line 277 checks to see if the string in countryCode is an 'o' or 'O'
// then does a multiplication problem
	 if (countyCode == "o" || countyCode == "O")
		 {
		 countyCode = "Orange County";
		 salesTax = (orangeCountyTax*(salesAmount-discount));
		 }
// line 285 checks to see if the string in countryCode is an 's' or 'S'
// then does a multiplication problem
	 else if (countyCode == "s" || countyCode == "S")
		 {
		 countyCode = "San Diego";
		 salesTax = (SanDiegoCountyTax*(salesAmount-discount));
		 }
// line 292 checks to see if the string in countryCode is an 'l' or 'L'
// then does a multiplication problem
	 else if (countyCode == "l" || countyCode == "L")
		 {
		 countyCode = "Los Angeles";
		 salesTax = (LosAngelesCountyTax*(salesAmount-discount));
		 }
// line 298 tell the user that they have made an invalid choice
	 else
		 {
			 cout << "You have reached a no no place";
		 }
	 return salesTax;
}

/*************************************************************************
 * CalcShipping
 * This function uses weight from the function GetSalesInfo to determine
 * how much to charge for the shipping
 ************************************************************************/
double CalcShipping(int weight)
{
	double ship ;       // flat rate of 5.00

// line 314 checks to see if the value in the variable weight falls in
// the range of 0 - 25
	 if (weight >= 0 && weight <= 25)
		{
		    ship = 5;
		}
// line 314 checks to see if the value in the variable weight falls in
// the range of 0 - 25
	 else if (weight >= 26 && weight <= 50)
		{
		    ship = ((weight - 25) * .10) + 5;
		}
// line 314 checks to see if the value in the variable weight falls in
// the range of 0 - 25
	else if (weight > 51)
		{
			ship = ((weight - 50) * .07) + 7.5;
		}
// line 257 tell the user that they have made an invalid choice
	else
		{
				 cout << "You have reached a no no place";
		}
	 return ship;
}

/*************************************************************************
 * OutputInvoice
 * This function has taken all the information from GetSalesInfo,
 * CalcDiscount, CalcSalesTax, CalcShipping , OutputInvoice and will
 * display it on a nice table style look.
 ************************************************************************/
void OutputInvoice(int accountNum, int month, int day , int year,
		           string countyCode, double salesAmount, int weight,
		           double discount,double salesTax, double ship, double
		           total)

{
// lines 353 - 367 will display a message to look like a receipt
	cout << endl;
	cout << setprecision(0)<<fixed;
	cout << "ACCOUNT NUMBER" << setw(33) << "COUNTY" << endl;
	cout << setw(9) << accountNum << setw(42) << countyCode << endl <<
			endl;
	cout << "DATE OF SALE: " << month <<"/"<< day << "/" << year << endl;
	cout << endl << endl;
	cout << setprecision(2)<<fixed;
	cout << "SALE AMOUNT: "<< setw(5) << "$" << setw(8)<< salesAmount <<
			endl;
	cout << "DISCOUNT: "<< setw(9) << "$" << setw(8)<< discount << endl;
	cout << "SALES TAX: "<< setw(8) << "$" << setw(8) << salesTax << endl;
	cout << "SHIPPING: "<< setw(9) << "$" << setw(8)<< ship << endl;
	cout << "TOTAL DUE: "<< setw(8) << "$" << setw(8)<< total << endl;
	cout << endl << endl;
}

