/*
 * File:   main.cpp
 * Author    : Andrew Lalyre
 * Student ID: 2534213
 * Lab #1    : Eclipse Lab
 * Class     : CSC5
 * Section   :TTH 1800 - 2110
 * Due Date  :9/3/2015
 *
 * Created on September 2, 2015, 2:28 PM
 */

#include <cstdlib>
#include <iomanip>
#include <iostream>

using namespace std;

/*
 *
 */
int main(int argc, char** argv) {

//    PROGRAMMER : Programmer's Name
//    CLASS      : Student's Course
//    SECTION    : Class Days and Time
//    LAB_NUM    : Lab Number
//    LAB_NAME   :Title if the lab

    const char PROGRAMMER[30] = "Andrew Lalyre";
    const char CLASS[5] = "CSC5";
    const char SECTION[25] ="TTH 1800 - 2110";
    const int  LAB_NUM = 1;
    const char LAB_NAME[17] = "Eclipse Tutorial";


    cout << left;
    cout << "***********************************************************";
    cout << "\n* PROGRAMMED BY : " << PROGRAMMER;
    cout << "\n* " << setw(14) << "CLASS" << ": " << CLASS;
    cout << "\n* " << setw(14) << "SECTION" << ": " << SECTION;
    cout << "\n* LAB#" << setw(9) << LAB_NUM << ": " <<LAB_NAME;
    cout << "\n***********************************************************\n\n";
    cout << right;

     return 0;


}

