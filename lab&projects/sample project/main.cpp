// Andrew Lalyre                   CSC5                    Chapter 2, p.83, #11
//
/*********************************************************
 *
 * COMPUTE DISTANCE PER TANK OF GAS
*_______________________________________________________________________________
*This program computes the distance a car can travel on one
*tank of gas based on gas tank capacity and average
*mile-per-gallon (MPG) rating for city and highway driving.
*
*Computation is based on the formula:
*Distance = Number of Gallons x Average Miles per Gallon
*_______________________________________________________________________________
*INPUT
*  tankCapacity   : Gas tak capacity in gallons
*  mpgCity        : Average MPG for city driving
*  mpgHighway     : Average MPG for hihgway driving
*
*OUTPUT
*  distance       :Distance car can travel
*
 *********************************************************/

#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    float tankCapacity;      //INPUT - Gas tank capacity in gallons
    float mpgCity;           //INPUT - Average MPG for city driving
    float mpgHighway;        //INPUT - Average MPG for highway driving
    float distance;          //OUTPUT - Distance car can travel
//
//  Initialize Program Variables
    tankCapacity = 20.0;
    mpgCity = 21.5;
    mpgHighway = 26.8;
//
//  Compute Distance Traveld City
    distance = tankCapacity * mpgCity;
//
//   Output Result
     cout << "The car can travel " << distance << " miles in town." << endl;
//
//  Compute Distance Traveled Highway
    distance = tankCapacity * mpgHighway;
//
//  Output Result
    cout << "the car can travel " << distance << " miles highway." << endl;
    return 0;
}




