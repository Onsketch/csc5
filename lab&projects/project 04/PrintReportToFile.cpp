#include "myheader.h"

/*******************************************************************************
 *  PrintReportToFile
 *-------------------------------------------------------------------------
 * The purpose for this function is to print a report of the names that were
 * found along with there id's and account balance. This will function will 
 * format the information into a receipt and tally everyone ones total. 
 ******************************************************************************/
void PrintReportToFile(int results,string fileName,string nameArray[],int
		idNumberArray[],double balanceArray[],int size)
{

	PrintHeaderToFile();
	 ofstream outfile;
//
//	    //open a file (step 1)
//	    outfile.open(outFileName.c_str());
//
//	    //user Input
//	    cin >> results;
//	    // Use a while loop to get cognitions input
//	    while ( num != -1)
//	    {
//	    // Writing to file mimics writing to cout
//	    // step 2
//	        outfile << num<< endl;
//
//	        // Prompt the user again
//	        cout << "Enter another number. -1 to quit: ";
//	        cin >> num;
//	    }
//	    // close the file
//	    outfile.close();

	    outfile << "ID # " << setw(8) << "NAME" << setw(32) << "BALANCE DUE";
	    outfile << "\n----" << setw(25) << "--------------------" << setw(16)
	    	 << "-----------" << endl;
	    outfile << left;
	    outfile << idNumberArray[size] << setw(9) << nameArray[size] <<
	    		setw(25);
	    outfile << balanceArray[size];

}