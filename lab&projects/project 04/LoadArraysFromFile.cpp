#include "myheader.h"

/*******************************************************************************
 * LoadArrayFromFile
 *-------------------------------------------------------------------------
 * The purpose for this function is to fill up three different arrays one of a 
 * string type the other of an int type and the last one of a double type.
 * the way in which it will fill up these arrays id by reading form a text file
 * and placing the values in their respective array.
 ******************************************************************************/
void LoadArrayFromFile(string name,int &size,string nameArray[],int
                       idNumberArray[],double balanceArray[],int records, int
                       &index,string fileName)
{
    ifstream infile;   //input for a file
    index =0;

    //step 1
    infile.open(fileName.c_str());

    // reads continuously until end of file
    while (!infile.eof())
    {
       	getline(infile, nameArray[index]);
       	infile >>idNumberArray[index];
       	infile >> balanceArray[index];
        cout << nameArray[index] << endl;
        	cout << idNumberArray[index] << " " <<
        	        balanceArray[index] << endl;
       	infile.ignore(1000,'\n');
       	index++;
       	records=index;
        size= records;
    }
    infile.close();
}
