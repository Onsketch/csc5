#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>


using namespace std;

void PrintHeaderToFile ();
/*******************************************************************************
 *  PrintHearderToFile
 *-------------------------------------------------------------------------
 * The purpose for this function is to print a header for this project.
 * The header will contain the the authors id and his class along with the
 * project name.
 *
 *  INPUT/OUTPUT
 *   "Words"
 * 
 * ****************************************************************************/

void LoadArrayFromFile(string,int &,string[],int[],double[],int,int &,string);
/*******************************************************************************
 * LoadArrayFromFile
 *-------------------------------------------------------------------------
 * The purpose for this function is to fill up three different arrays one of a 
 * string type the other of an int type and the last one of a double type.
 * the way in which it will fill up these arrays id by reading form a text file
 * and placing the values in their respective array.
 * INPUT/OUTPUT
 *  string name               // variable that holds a name
 *  int size                  // the subscript of the arrays
 *  string nameArray[]        // an array that will be filled up with names
 *  int idNumberArray[]       // an array that will be filled with id numbers
 *  double balanceArray[]     // an array that will be filled with a money 
 *                            // amount
 *  int records               // a counter to count how many lines were read 
 *                            // from a file
 *  int index,                // the subscript of the arrays
 *  string fileName           // what text document will be read to fill the 
 *                            // arrays with 
 * ****************************************************************************/

int SearchForMatch(string [],int,int ,string);
/*******************************************************************************
 * SearchForMatch
 *-------------------------------------------------------------------------
 * The purpose for this function is to have the user enter a name and check
 * whether or not the name exist in the file provided. This function will also
 * store the variable if it is found into another array 
 * INPUT
 *  string nameArray[]          // an array that will be filled up with names
 *  int size                    // the subscript of the arrays
 *  string searchName           // variable that holds a name that will be 
 *                              // searched for
 *  int printIdNumberArray[]    // an array that will be filled by the matched 
 *                              // results
 *  string printNameArray[]     // an array that will be filled by the matched 
 *                              // results
 *  double printBalanceArray[]  // an array that will be filled by the matched 
 *                              // results
 *  int printSize               // array subscript
 *  
 * OUTPUT                       
 *  int index,                  // the subscript of the arrays
 *****************************************************************************/

void PrintReportToFile(int,string,string[],int[],double[],int);
/*******************************************************************************
 *  PrintReportToFile
 *-------------------------------------------------------------------------
 * The purpose for this function is to print a report of the names that were
 * found along with there id's and account balance. This will function will 
 * format the information into a receipt and tally everyone ones total. 
 * INPUT
 *  int results                 // subscript value
 *  string fileName             // the file that is being read from
 *  int printSize               // the array size
 * 
 * OUTPUT
 *  int printIdNumberArray[]    // an array of id numbers
 *  string printNameArray[]     // an array of names
 *  double printBalanceArray[]  // an array of account balances
 * 
 ******************************************************************************/

