#include "myheader.h"

/*******************************************************************************
 *  PrintHearderToFile
 *-------------------------------------------------------------------------
 * The purpose for this function is to print a header for this project.
 * The header will contain the the authors id and his class along with the
 * project name.
 ******************************************************************************/
void PrintHeaderToFile ()
{
    cout << left;
    cout << "*******************************************************";
    cout << "\n* PROGRAMMED BY : " << "Andrew Lalyre";
    cout << "\n* " << setw(14) << "CLASS" << ": " << "CSC5";
    cout << "\n* " << setw(14) << "SECTION" << ": " << "TTH 1800 - 2110";
    cout << "\n* Project # " << "    : " << "04";
    cout << "\n*******************************************************\n\n";
    cout << right;
}
