/**************************************************************************
 * Programmed By : Andrew Lalyre
 * Class         : CSC5
 * Section       : TTH - 1800-1930
 * project 3     : Parallel Arrays
 *************************************************************************/

#include "myheader.h"

int main(){

    string name ;             // will get the number that is in the file
    int size = 100;           // will be used as a counter.
    string nameArray[size];   // an array that will be filled up with names
    int idNumberArray[size];  // an array that will be filled with id numbers
    double balanceArray[size];//an array that will be filled with a money 
                              // amount
    int records = 0;          // a counter to count how many lines were read 
                              // from a file
    int index ;            // the subscript of the arrays
    int result;               // holds the subscript value of an array
    string searchName;        // variable that holds a name that will be 
                              // searched for
    string fileName;          // what text document will be read to fill the 
                              // arrays with 
    string outFileName;       // what text document will be written to 
//    int printSize = 100;      // array subscript
//    int printIdNumberArray[printSize];   // an array that will be filled by the matched 
//                                         // results
//    string printNameArray[printSize];    // an array that will be filled by the matched 
//                                         // results
//    double printBalanceArray[printSize]; // an array that will be filled by the matched 
//                                         // results

    // prompts the user what file they want to read from
    cout << "What input file would you like to use? ";
    cin >> fileName;
    // prompts the user what file they want to write to
    cout << "What output File would you like to use? ";
    cin >> outFileName;
    cin.ignore(1000,'\n');
    cout << endl;

    // this is a function that will fill up 3 different arrays
    LoadArrayFromFile(name,size,nameArray,idNumberArray,balanceArray,records,
                      index,fileName);

//    // this for loop will make copy of an array         
//    for (index = 0 ; index < size ; index++)
//    {
//        printNameArray[index] = nameArray[index];
//        printIdNumberArray[index] = idNumberArray[index]; 
//        printBalanceArray[index] = balanceArray[index];
//
//    }
        
//    // this do while loop is meant to allow the user to keep searching for names
    do
    {
	cout << "Who do you want to search for (enter done to exist): ";
	getline(cin,searchName);
	result=SearchForMatch(nameArray,index,size,searchName);

	if(result >= 0)
	{
            cout << "Found." << endl << endl;
	}
	else if (searchName == "done")
	{
            cout << "\nThank you for using my program";
	}
	else
	{
            cout << searchName;
            cout << " was not found." << endl << endl;
	}
    }while (searchName != "done");
//
//        // this function is meant to print the information gathered in this 
//        // program into a nice receipt form
//	PrintReportToFile(result,outFileName,nameArray,idNumberArray,
//					  balanceArray,size);

    return 0;
}
