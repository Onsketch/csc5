/**************************************************************************
 * Programmed By : Andrew Lalyre
 * Class         : CSC5
 * Section       : TTH - 1800-1930
 * project 2     : Repetition & Switch Statements
 *************************************************************************/

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <iomanip>
int func(char userInput);
using namespace std;

/**************************************************************************
 *
 * Compute Grade points and G.P.A
 * ________________________________________________________________________
 *
 * ________________________________________________________________________
 * INPUT
 *
 *
 * OUTPUT
 *
 *
 *************************************************************************/
int main()
{
    /**********************************************************************
     * CONSTANTS
     * --------------------------------------------------------------------
     *
     *********************************************************************/

 char userInput;

//lines 64 - 71 are used as an identifier of Author to this class
//project program
cout << left;
cout << "*******************************************************";
cout << "\n* PROGRAMMED BY : " << "Andrew Lalyre";
cout << "\n* " << setw(14) << "CLASS" << ": " << "CSC 5";
cout << "\n* " << setw(14) << "SECTION" << ": " << "TTH 1800-2110";
cout << "\n* " << setw(14) << "Project # " << ": " <<"02";
cout << "\n*************************************************\n\n";
cout << right;
// the for loop is to make sure program will only run 3 times
for (int i=1; i<4;i++)
{
 int count2 = 0;
 double count3 = 0.0;
 double gpa;
 cout << "Test #" << i << ":" << endl;
 cout << endl;

  // the do while loop is being used to ensure that the user gets to
  // input at least once.
  do
  {
    cout << "Enter a Letter Grade (enter 'X' to exit): ";
    cin >> userInput;

      // the while loop is only being used for input validation.
      while (userInput!='A' && userInput!='a' && userInput!='B' &&
	     userInput!='b' && userInput!='C' && userInput!='c' &&
	     userInput!='D' && userInput!='d' && userInput!='F' &&
	     userInput!='f' && userInput !='X' && userInput !='x')
      {
	cout << "\n Invalid letter grade, please try again.\n";
	cout << "\n Enter Letter Grade (enter 'X' to exit):";
	cin >> userInput;
      }

      if(userInput !='X' && userInput !='x')
      {
    int grade=func(userInput);

    //line number 80 will add the values of the userInput together
    count2+=grade;

    // line 83 is a counter that holds the number of times the loop
    // as execute.
    count3++;
      }
    cout << fixed << showpoint << setprecision(2);
    // line 88 will get a grade point average by dividing count3
    // by count2


  } while(userInput !='X' && userInput!='x');

	 cout << endl;
	 cout << "Total Grade Points: " << count2 << endl;
	 cout << "GPA: " << gpa << endl;
	 cout << endl;
	 cout << endl;

}

     return 0;
}


int func(char userInput)
{
    int grade=0;
    userInput=toupper(userInput);
    int value=userInput; // if input='A' then value = 65

    grade=69-value;   // gpa=4

    if(grade<0)grade=0;
    return grade;
}
