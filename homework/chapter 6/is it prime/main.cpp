//Andrew Lalyre                      CSC5                 Chapter 6, p. 375, #21
//
/*******************************************************************************
 *
 * Is It Prime
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>

using namespace std;
void isPrime(int);

int main() {

   int num;
  
   cout << "Please enter a number to find out if it is prime: ";
   cin  >> num;

    isPrime(num);
            
    return 0;
}

void isPrime(int num)
{ 
    int startingPoint = 2;
    while (startingPoint < num)
   {
      if (num % startingPoint == 0)
      {
         cout << "The number is not prime because it is divisiable by "
              << startingPoint;
         break;
      }
      ++startingPoint;
   }

   if (startingPoint == num)
         cout << "This number is prime.";
}
