//Andrew Lalyre                      CSC5                 Chapter 6, p. 375, #21
//
/*******************************************************************************
 *
 * Prime number list
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/
#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

void calculateRetail(int,double);

int main() {

    int cost;
    double markup;
    
    cout << "Please enter an item's wholesale cost: ";
    cin >> cost;
    while (cost < 0)
      {
	cout << "You can not input a negative number: ";
	cin >> cost;
      }
    cout << "Please enter markup percent in decimal form: ";
    cin >> markup;
    while (markup < 0)
      {
	cout << "You can not input a negative number: ";
	cin >> markup;
      }
    calculateRetail(cost,markup);
    return 0;
}
void calculateRetail(int a,double b)
{
    double retailPrice;
    
    retailPrice = (a * b) + a ;
    
    cout << fixed << showpoint << setprecision(2);
    cout << "The retail price of this item is: $" << retailPrice ;   
}

