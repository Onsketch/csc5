//Andrew Lalyre                      CSC5                 Chapter 6, p. 375, #21
//
/*******************************************************************************
 *
 * Prime number list
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;
void isPrime(int);
void output(int);

int main() {
    for ( int i = 0; i <= 100; i++)
    {
     isPrime(i);
    }           
    return 0;
}

void isPrime(int i)
{ 
    int startingPoint = 2;

    while (startingPoint < i)
   {
      if (i % startingPoint == 0)
      {
         // if number is prime it would be displayed here
         break;
      }
      ++startingPoint;
   }

   if (startingPoint == i)
   {
       cout << i << " is number is prime." << endl;
   
    output (i);
   }

}

void output(int i)
{
    // write to a file
    ofstream outfile;
    
    //open a file (step 1)    
    outfile.open("prime_List");
    
    //user Input
   
    int num;
    num = i;

   
    // Writing to file

    outfile << num << endl;
        
      // close the file
    outfile.close();
}
