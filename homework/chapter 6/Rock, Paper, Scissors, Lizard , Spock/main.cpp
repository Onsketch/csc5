//Andrew Lalyre                      CSC5                 Chapter 6, p. 375, #23
//
/*******************************************************************************
 *
 * Row-Sham-Bow
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/
#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

void playerWins(int,int);
void computerWins(int,int);
void tie(int,int);

int main() {

    string userInput;
    int x,ai1=0;
    
    cout << "Welcome to Paper, Scissors, Rock, Lizard, Spock. " << endl;
    cout << "Rules of the Game are :" << endl;
    cout << "Scissors cuts Paper. Paper covers Rock. ";
    cout << "Rock crushes Lizard. ";
    cout << "Lizard poisons Spock." << endl;
    cout << "Spock smashes Scissors. ";
    cout << "Scissors decapitates Lizard. Lizard eats Paper. ";
    cout << "Paper Disproves Spock." << endl;
    cout << "Spock Vaporizes Rock. ";
    cout << "And as always Rock crushes Scissors." << endl;
    cout << endl;
    
   cout << "Press 1 for Rock, 2 for Paper, 3 for Scissors," << endl;
   cout << "4 for Lizard, 5 for Spock." << endl;
    
       
   while(userInput != "n" && userInput != "N" && userInput !="no"
          && userInput != "NO")
    {
   
    srand(time(0));
    ai1 = rand()% 5+1 ;
       
   cin >> x;
   
   if (x >= 1 && x <= 5)
   {
       playerWins(x,ai1);
       computerWins(x,ai1);
       tie(x,ai1);
   }
   
   else
   {
       cout << "You have Chosen to play the game incorrectly. " << endl;
       cout << "A band of warriors are coming for you." << endl;
   }
       
    cout << "\nPlay again?";
    cin >> userInput;
    }
       return 0;
}

void tie(int x,int ai1)
{
    if ((x == 1 && ai1 == 1) ||
        (x == 2 && ai1 == 2) ||
        (x == 3 && ai1 == 3) ||
        (x == 4 && ai1 == 4) ||
        (x == 5 && ai1 == 5))
    {
        cout << x << " " << ai1;
        cout << "\nBoth Players tie." << endl;
    }
}

void playerWins(int x,int ai1)
{
    if ((x == 3 && ai1 == 2) ||
             (x == 2 && ai1 == 1) ||
             (x == 1 && ai1 == 4) ||
             (x == 4 && ai1 == 5) ||
             (x == 5 && ai1 == 3) ||
             (x == 3 && ai1 == 4) ||
             (x == 4 && ai1 == 2) ||
             (x == 2 && ai1 == 5) ||
             (x == 5 && ai1 == 1) ||
             (x == 1 && ai1 == 3))
    {
        cout << x << " " << ai1;
        cout << "\nPlayer 1 wins. " << endl;
    }
}

void computerWins(int x,int ai1)
{
    if ((x == 2 && ai1 == 3) ||
          (x == 1 && ai1 == 2) ||
          (x == 4 && ai1 == 1) ||
          (x == 5 && ai1 == 4) ||
          (x == 3 && ai1 == 5) ||
          (x == 4 && ai1 == 3) ||
          (x == 2 && ai1 == 4) ||
          (x == 5 && ai1 == 2) ||
          (x == 1 && ai1 == 5) ||
          (x == 3 && ai1 == 1))
    {
        cout << x << " " << ai1;
        cout << "\nPlayer 2 wins." << endl;
    }
}

