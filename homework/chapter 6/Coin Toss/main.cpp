//Andrew Lalyre                      CSC5                 Chapter 6, p. 375, #23
//
/*******************************************************************************
 *
 * Coin Toss
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/
#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

void coinToss(int);

int main() {

    int flips = 0;
    srand(time(0)); 
    
    cout << "Please enter how many coin flips you would like to do: ";
    cin >> flips;
    
    for ( int i = 0; i <= flips; i++)
    {
     coinToss(i);
    } 
    return 0;
}

void coinToss(int a)
{
    
    a = rand()% 2+1 ;
    if (a == 1)
    {
        cout << "Heads" << endl;
    }
    
    else if (a == 2)
    {
        cout << "Tails" << endl;
    }
    
    else
    {
        cout << "Game is broken please call someone" << endl;
    }
}

