//Andrew Lalyre                      CSC5                  Chapter 7, p. 444, #1
//
/*******************************************************************************
 *
 * Biggest to smallest int sort Array 
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
    int value[10];
    int startingPoint = 0;
    
    cout << "Please enter 10 numbers: ";
    for(int i = 0; i < 10; i++)
    {
       cin >> value[i];
    }
    
      //We declare a bool here to decide if the loop is completed or not, it's our way of running a nested loop until the nested loop no longer needs running
    bool swapped=true;
    //initialize this bool to true
    while(swapped){
        //while this bool is true
        swapped=false;
        //Set the bool to false so that if this loop doesn't hit that if statment, it won't run anymore, without this it will be an endless loop
        for(int i=1; i<10; i++){
            //iterate through the array
            if (value[i] < value[i-1]){
                //This if statement compares the values at i, and i-1, if the value at i-1 is smaller..
                startingPoint = value[i];
                //We will first set startingPoint = the current value of value[i].  We need to do this because we can't* swap 2 variables without something to hold one of them
                //* -  The above is technically a lie, but for the sake of coding theory, we cannot.  Any function we use will employ this same basic theory.  
                value[i] = value[i-1];
                //Set the current value of value[i] to the value of value[i-1]
                //(so first iteration through it will be 90 vs 10, which is false, but then 50 vs 90 is not)
                value[i-1]= startingPoint;
                //This completes the 'swap' when both values have been assigned to their new location.
                swapped = true;
                //Since values are still being swapped here, we set this to true again, so the loop will continue to run until it checks out as being 'false'
            }
        }
    }
    for (int i = 0; i < 10; i++)
    {
        cout << value[i] << " " ;
//        if (value[i] > startingPoint)
//        {
//            startingPoint = value[i];
//            cout << startingPoint << " " ;
//        }
    }

    return 0;
}

