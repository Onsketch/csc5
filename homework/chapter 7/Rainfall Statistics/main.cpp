//Andrew Lalyre                      CSC5                  Chapter 7, p. 446, #2
//
/*******************************************************************************
 *
 * Driver License Exam
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <cstring>

using namespace std;

int main() {
    const int SIZE = 12;
    double rainfall[SIZE];
    double count = 0;
    double avg = 0;
    double highest = 0 ;
    double lowest = 1000000000000000000000000000000;
        
    for(int i = 0; i < SIZE; i++)
    {
       cout << "please enter the rainfall for the month: ";
        cin >> rainfall[i];
        count +=rainfall[i];
    }
    
    cout << "\nTotal rainfall for the year is " << count << " inches" << endl;
    avg = count / 12;
    cout << "The average monthly rainfall is " << avg << " inches" << endl;
   
    for (int i = 0; i < SIZE; i++)
    {
        if (rainfall[i] > highest)
        {
            highest = rainfall [i];
        }
    }
    
    cout << "The highest rainfall amount was " << highest << " inches" << endl;
    
       for (int i = 0; i < SIZE; i++)
    {
        if (rainfall[i] < lowest)
        {
            lowest = rainfall [i];
        }
    }
    
    cout << "The lowest rainfall amount was " << lowest << " inches" << endl;

    return 0;
}

