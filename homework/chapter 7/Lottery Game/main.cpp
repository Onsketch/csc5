//Andrew Lalyre                      CSC5                 Chapter 7, p. 447, #13
//
/*******************************************************************************
 *
 * Lottery Game
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

int main() {

    const int SIZE = 5;
    int userPick[SIZE];
    int lottery[SIZE];
    int count = 0;
    int i;
    srand(time(0));
    
    cout << "Welcome to Onsketch Lottery please enter 5 numbers from 1 to 10:"
            " \n";
    for(int i = 0; i < SIZE; i++)
    {
       cin >> userPick[i];
    }
   
    cout << endl << "Lottery numbers are : \n";
    for (int i=0; i< SIZE;i++)
    {
      lottery[i] = rand()% 10+1;
      cout << lottery[i] << " "; 
    }
    
    cout << endl << "Your numbers are : \n";
    for (int i = 0; i < SIZE; i++)
    {
        cout << userPick[i] << " " ;

    }
    
    for (int i=0; i< SIZE;i++)
    {
        if (userPick[i] == lottery[i])
        {
            count++;
        }
    }
    cout << endl;
    cout << "you have a match " << count << " of the lottery numbers" << endl;
    return 0;
}
