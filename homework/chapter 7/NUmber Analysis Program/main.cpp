//Andrew Lalyre                      CSC5                  Chapter 7, p. 445, #6
//
/*******************************************************************************
 *
 * Number Analysis
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main() {

    double sum = 0;        // this will be used to add up the values in the file
    int num = 0;        // will get the number that is ibn the file
    int count = 0;      // will be used as a counter.
    int array[count];   //
    int highest = 0;    //
    int lowest = 1000000000000000000000000000000;     //
    double avg = 0.0;     //
    
   
    // input for a file
    ifstream infile;
    //step 1
    infile.open("numberList.txt");
    
  // reads continously until end of file
        while (infile >>num)
        {
            sum += num;
            count++;
    
    for(int i = 0; i < count; i++)
    {
       array[i] = num ;
    }
    for (int i = 0; i < count; i++)
    {
        if (array[i] > highest)
        {
            highest = array [i];
        }
    }
    
    for (int i = 0; i < count; i++)
    {
        if (array[i] < lowest)
        {
            lowest = array [i];
        }
    }
    
    avg = sum/count;
        }
    cout << "The lowest number is " << lowest << endl;
    cout << "The highest number is " << highest << endl;
    cout << "total of all the numbers is : " << sum << endl;
    cout << "The average of the numbers is " << avg << endl;
        
        
    return 0;
}

