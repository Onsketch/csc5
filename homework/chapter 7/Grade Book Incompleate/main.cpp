//Andrew Lalyre                      CSC5                 Chapter 7, p. 444, #11
//
/*******************************************************************************
 *
 * Grade Book
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

int main() {

    string studentName[5];
    char letterGrade[4];
    
     // this for loop will gather the names of the students
    cout << "Please enter student name: ";
    for(int i = 0; i < 5; i++)
    {
       getline(cin,studentName[i]);
      // this for loop will gather the letter grade scores for each student
       cout << "Please enter the Letter grade: ";
       for (int i = 0; i < 4; i++)
       {
           cin >> letterGrade;
       }
    }
    

     // this is to display the students name
//      for (int i = 0; i < 5; i++)
//    {
//        cout << studentName[i] << " " ;
//
//    }

    return 0;
}

