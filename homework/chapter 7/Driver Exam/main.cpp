//Andrew Lalyre                      CSC5                  Chapter 7, p. 446, #9
//
/*******************************************************************************
 *
 * Driver License Exam
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <cstring>

using namespace std;

int main() {
    const int SIZE = 20;
    char exam[SIZE] = {'B','D','A','A','C','A','B','A','C','D','B','C','D','A',
    'D','C','C','B','D','A'};
    char userExam[SIZE];
    int count=0;
    
    cout << "Please enter your 20 choices: ";
    for(int i = 0; i < SIZE; i++)
    {
       cin >> userExam[i];
       userExam=toupper(userExam);
       while (userExam!='A' && userExam!='B' && userExam!='C' &&
              userExam!='D')
       {
           cout << "The only Choices you can make are A,B,C,D ";
           cin >> userExam[i];
       }
    }
       
     for (int i=0; i< SIZE;i++)
    {
        if (userExam[i] == exam[i])
        {
            count++;
        }
    }
        if (count >= 15)
        {
            cout << "You passed the exam. YAY!\n";
        }
        if (count < 15)
        {
            cout << "You did not pass.\n";
        }
        
        cout << "You got " << count << " correct answers." << endl;
        count = count - 20;
        count << "You got " << count << "incorrect answers." << endl;
    
    return 0;
}

