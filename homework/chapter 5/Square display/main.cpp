
//Andrew Lalyre                      CSC5                 Chapter 5, p. 298, #22
//
/*******************************************************************************
 *
 *Square Display
 *______________________________________________________________________________
 * This program will get a number from one and fifteen and then display a patter
 * equal to the inputed number
 *
 *______________________________________________________________________________
 * INPUT
 *   num
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/
#include <iostream>
using namespace std;

int main() {
    int num;
    
    cout << "Please enter a number less than 15: ";
    cin >> num;
    
    if(num>=0 && num<=15)
    {
       for(int i=0; i<num;i++)
       {
          for(int j=0;j<num;j++)
          {
           cout << "x";
          }
          cout << endl;
       }
    }
    else
    {
        cout << "Your lack of reading skills will bring you shame.";     
    }
    return 0;
}