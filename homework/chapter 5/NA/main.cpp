//Andrew Lalyre                      CSC5                  Chapter 5, p. 294, #5
//
/*******************************************************************************
 *
 * Membership Fees Increase
 *______________________________________________________________________________
 * This program is intended to display the rising cost of membership fees at a
 * country club over the next 6 year.
 *
 *______________________________________________________________________________
 * INPUT
 *   price
 *   percent
 *   
 * OUTPUT
 *   increase
 *   newPrice 
 *   total
 * 
 ******************************************************************************/
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <iomanip>
using namespace std;

int main() {

    int price = 2500;
    double percent = .04;
    double increase ;
    double newPrice ;
    double total;
    cout << "The current price for a yearly membership at a \ncount club is $"
            << price << " and it will go up by " << percent << "% \nevery year "
            "for the next six years. here are the new prices:" << endl;
    cout << endl;
    for (int i=1;i<7;i++)
    {
        
       increase = price*percent;
       newPrice = price+increase;
      
       cout << fixed << showpoint << setprecision(2);
       cout << "After year " << i << " the price will be $" << newPrice << endl;
       price=+price;
    }
    return 0;
}


