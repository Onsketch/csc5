//Andrew Lalyre                      CSC5                 Chapter 5, p. 298, #23
//
/*******************************************************************************
 *
 * Pattern Displays
 *______________________________________________________________________________
 * This program will display a triangle using the + symbol and will also
 *  display it upside down
 *
 *______________________________________________________________________________
 * INPUT
 *   i
 *   j
 *   
 * OUTPUT
 *   '*'
 * 
 ******************************************************************************/
#include <iostream>
#include <iomanip>
using namespace std;

int main() {
 for (int i=0; i <10; i++)
 {
 	for(int j=0; j<i;j++)
 	{
 		cout << "+";
 	}
 	cout << endl;
 }
 cout << endl;

 for (int i=9;i>0;i--)
 {
	for (int j=0;j<i;j++)
	{
	 cout << "+";
	}
	cout << endl;
 }

 return 0;
}
