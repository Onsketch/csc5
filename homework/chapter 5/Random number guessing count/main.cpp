//Andrew Lalyre                      CSC5                 Chapter 5, p. 298, #21
//
/*******************************************************************************
 *
 * Random Number Guessing Game Part Two
 *______________________________________________________________________________
 * This program will generate a random number from 0 to 100 and the user has to
 * guess the number. This program will also let the user know how many times
 * they have guessed.
 *
 *______________________________________________________________________________
 * INPUT
 *   userInput
 *   choossenNumber
 *   num
 *   
 * OUTPUT
 *   text
 *   count
 * 
 ******************************************************************************/
#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

int main() {

    // User input uses while or do-while loop
    // Single variable for loop
    string userInput;
    int count =1;
    cout << "Would you like to play? please enter yes: ";
    cin >> userInput;
    
    while (userInput == "yes")
    {
        // srand(time(0) is used to get a random number
        srand(time(0));
        int chosenNumber = rand() % 100;
        
        // The actual game
        cout << "Enter a number: ";
        int num;
        cin >> num;
        
        do 
        {
            if (num > chosenNumber)
            {
                cout << "Too High! Try again: ";
                cin >> num;
            }
            else if (num < chosenNumber)
            {
                cout << "Too low! Try again: ";
                cin >> num;
            }
        count++;
        } while (num != chosenNumber);
      
      cout << "Correct ";
      cout << "you guessed " << count << " times." << endl;
      
      cout << endl;
      // Prompt the user to play again
      cout << "Would you like to play again?";
      cin >> userInput;
    }
    
    return 0;
}


