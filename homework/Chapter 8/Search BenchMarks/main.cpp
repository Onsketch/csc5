//Andrew Lalyre                      CSC5                  Chapter 8, p. 488, #8
//
/*******************************************************************************
 *
 * Search comparison 
 *______________________________________________________________________________
 * This program will allow the user to enter an account number and the program
 * will check if the numbered entered exist. 
 *
 *______________________________________________________________________________
 * INPUT
 *   array
 *   accountNumber
 *   
 * OUTPUT
 *   result
 * 
 *******************************************************************************
 * 
 * linearSearch
 * ------------------------------------------------------------------------
 * The purpose of this function is to search the an array for a number that was
 * entered by the user. This function is also a search algorithm.
 *
 * Input
 *  array
 *  accountNumber
 *  SIZE
 *
 * Output
 *  i
 *
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

// linear Search function prototype
int linearSearch(int [], int ,int);
// sorts the array
int sortArray(int [], int);
// binary Search function prototype
int binarySearch(int [], int ,int);

int const SIZE = 20;      // Size of Array
int main() {

    int num = 0;        // will get the number that is in the file
    int array[SIZE];    //
    int keyNumber;
    int result;
    
    cout << "Please enter 20 numbers: ";
    for(int i = 0; i < SIZE; i++)
    {
       cin >> array[i];
    }
    
    cout << "Please enter the number you want to find? : " ;
    cin >> keyNumber;
    
    result = linearSearch(array,SIZE,keyNumber);
    sortArray(array,SIZE);
    result = binarySearch(array,SIZE,keyNumber);
    
    // if the results contain a -1 the wining numbers were not found
    if(result == -1)
    {
        cout<<"Nothing was Found"<<endl;
    }
    else
    {
        // the number was found.
        cout << "Account number found.";
    }

    return 0;
}

int linearSearch(int array[], int SIZE,int accountNumber)
{
  
	for(int i=0; i<SIZE;i++){
		if(accountNumber==array[i]){
        return i;
		}
	}
	return -1;

}

int sortArray(int array[], int SIZE)
{
     int startingPoint = 0;
        
// We declare a bool here to decide if the loop is completed or not, it's our
// way of running a nested loop until the nested loop no longer needs running
    bool swapped=true;

// initialize this bool to true
    while(swapped)
    {

// while this bool is true
        swapped=false;

// Set the bool to false so that if this loop doesn't hit that if statment, it 
// won't run anymore, without this it will be an endless loop
        for(int i=1; i<10; i++)
        {
// iterate through the array
            if (array[i] < array[i-1])
            {
// This if statement compares the values at i, and i-1, if the value at i-1 is
// smaller
                startingPoint = array[i];
// We will first set startingPoint = the current value of value[i].  We need to 
// do this because we can't swap 2 variables without something to hold one of 
// them in   
                array[i] = array[i-1];
//Set the current value of value[i] to the value of value[i-1]
                array[i-1]= startingPoint;
//This completes the 'swap' when both values have been assigned to their new 
// location.
                swapped = true;
//Since values are still being swapped here, we set this to true again, so the
// loop will continue to run until it checks out as being 'false'
            }
        }
    }
}

int binarySearch(int array[], int SIZE,int numFinder)
{
    
    int first = 0;         // First array element
    int last = SIZE - 1;  // Last array element 
    int middle;            // midpoint of search
    int position = -1;     // position of search value
    bool found = false;    // Flag
    
    while (! found && first <= last)
    {
        middle = (first + last) / 2;    // calculate midpoint
        if (array[middle] == numFinder) // if numFinder is found at mid
        {
            found = true;
            position = middle;
        }
        else if (array[middle] > numFinder)  // if numFinder is in lower half
        {
            last = middle - 1;
        }
        else
        {
            first = middle + 1;             // if numFinder is in upper half
        }
    }
    return position;
}