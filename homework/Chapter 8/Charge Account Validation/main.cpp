//Andrew Lalyre                      CSC5                  Chapter 8, p. 487, #1
//
/*******************************************************************************
 *
 * Charge Account validation
 *______________________________________________________________________________
 * This program will allow the user to enter an account number and the program
 * will check if the numbered entered exist. 
 *
 *______________________________________________________________________________
 * INPUT
 *   array
 *   accountNumber
 *   
 * OUTPUT
 *   result
 * 
 *******************************************************************************
 * 
 * linearSearch
 * ------------------------------------------------------------------------
 * The purpose of this function is to search the an array for a number that was
 * entered by the user. This function is also a search algorithm.
 *
 * Input
 *  array
 *  accountNumber
 *  SIZE
 *
 * Output
 *  i
 *
 ******************************************************************************/

#include <iostream>

using namespace std;

int linearSearch(int array[], int size,int accountNumber);

int main() {

    /**********************************************************************
     * CONSTANTS
     * --------------------------------------------------------------------
     *  SIZE         // will be used as a size for array
     *********************************************************************/
    int const SIZE = 18;
    int array[SIZE] = {5658845,8080152,1005231,4520125,4562555,6545231,7895122,
                       5552012,3852085,8777541,5050552,7576651,8451277,7825877,
                       7881200,1302850,1250255,4581002};
    int accountNumber;
    int result;
    
    cout << "Please enter the account number you want to look up?:" ;
    cin >> accountNumber;
    
    result = linearSearch(array,SIZE,accountNumber);
    
    if(result >= 0)
    {
        cout << "Account number found.";
    }
    else
    {
        cout<<"Nothing was Found"<<endl;
    }
    return 0;
}

/*************************************************************************
 * linearSearch
 * This function will attempt to find a number.
 ************************************************************************/
int linearSearch(int array[], int SIZE,int accountNumber)
{
    for(int i=0; i<SIZE;i++)
    {
        if(accountNumber==array[i])
        {
            return i;
        }
    }
    return -1;
}
