//Andrew Lalyre                      CSC5                  Chapter 7, p. 487, #4
//
/*******************************************************************************
 *
 * Charge Account validation pt 2
 *______________________________________________________________________________
 * This program will allow the user to enter an account number and the program
 * will check if the numbered entered exist. 
 *
 *______________________________________________________________________________
 * INPUT
 *   array
 *   accountNumber
 *   
 * OUTPUT
 *   result
 * 
 *******************************************************************************
 * 
 * linearSearch
 * ------------------------------------------------------------------------
 * The purpose of this function is to search the an array for a number that was
 * entered by the user. This function is also a search algorithm.
 *
 * Input
 *  array
 *  accountNumber
 *  SIZE
 *
 * Output
 *  i
 *
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

// binary Search function prototype
int linearSearch(int [], int ,int);
int const count = 18;      // will be used as a counter.
   
int main() {

    int result;        // to hold search results
    // array with lottery ticket numbers.
    int array[count] = {5658845,8080152,1005231,4520125,4562555,6545231,7895122,
                        5552012,3852085,8777541,5050552,7576651,8451277,7825877,
                        7881200,1302850,1250255,4581002};
    int accountNumber; // holds the wining lottery number.
    int startingPoint = 0;
    
    // get the winning lotto number to search for
    cout << "Please enter the account number you want to look up?:" ;
    cin >> accountNumber;
    
    //We declare a bool here to decide if the loop is completed or not, it's our way of running a nested loop until the nested loop no longer needs running
    bool swapped=true;
    //initialize this bool to true
    while(swapped){
        //while this bool is true
        swapped=false;
        //Set the bool to false so that if this loop doesn't hit that if statement, it won't run anymore, without this it will be an endless loop
        for(int i=1; i<10; i++){
            //iterate through the array
            if (array[i] < array[i-1]){
                //This if statement compares the values at i, and i-1, if the value at i-1 is smaller..
                startingPoint = array[i];
                //We will first set startingPoint = the current value of value[i].  We need to do this because we can't* swap 2 variables without something to hold one of them
                //* -  The above is technically a lie, but for the sake of coding theory, we cannot.  Any function we use will employ this same basic theory.  
                array[i] = array[i-1];
                //Set the current value of value[i] to the value of value[i-1]
                //(so first iteration through it will be 90 vs 10, which is false, but then 50 vs 90 is not)
                array[i-1]= startingPoint;
                //This completes the 'swap' when both values have been assigned to their new location.
                swapped = true;
                //Since values are still being swapped here, we set this to true again, so the loop will continue to run until it checks out as being 'false'
            }
        }
    }
    
    // search for lotto
    result = linearSearch(array,count,accountNumber);
    
    // if the results contain a -1 the wining numbers were not found
    if(result == -1)
    {
        cout<<"Nothing was Found"<<endl;
    }
    else
    {
        // the number was found.
        cout << "Account number found.";
    }
   
                   
    return 0;
}
int linearSearch(int array[], int size,int accountNumber)
{
    
    int first = 0;         // First array element
    int last = count - 1;  // Last array element 
    int middle;            // midpoint of search
    int position = -1;     // position of search value
    bool found = false;    // Flag
    
    while (! found && first <= last)
    {
        middle = (first + last) / 2;    // calculate midpoint
        if (array[middle] == accountNumber) // if numFinder is found at mid
        {
            found = true;
            position = middle;
        }
        else if (array[middle] > accountNumber)  // if numFinder is in lower half
        {
            last = middle - 1;
        }
        else
        {
            first = middle + 1;             // if numFinder is in upper half
        }
    }
    return position;
}	