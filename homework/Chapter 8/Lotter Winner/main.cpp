//Andrew Lalyre                      CSC5                  Chapter 8, p. 487, #2
//
/*******************************************************************************
 *
 *lottery Winner Checker
 *______________________________________________________________________________
 * This program will allow the user to enter an account number and the program
 * will check if the numbered entered exist. 
 *
 *______________________________________________________________________________
 * INPUT
 *   array
 *   accountNumber
 *   
 * OUTPUT
 *   result
 * 
 *******************************************************************************
 * 
 * linearSearch
 * ------------------------------------------------------------------------
 * The purpose of this function is to search the an array for a number that was
 * entered by the user. This function is also a search algorithm.
 *
 * Input
 *  array
 *  accountNumber
 *  SIZE
 *
 * Output
 *  i
 *
 ******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;
int linearSearch(int array[], int size,int numFinder);

int main() {

    int num = 0;        // will get the number that is in the file
    int const count = 10;      // will be used as a counter.
    int array[count] = {13579,62483,26791,77777,26792,79422,33445,85647,55555,
                        93121};   //
    int numFinder;
    
    cout << "Please enter the lottery numbers: ";
    cin >> numFinder;
    
    int result = linearSearch(array,count,numFinder);
	if(result>=0)
        {
            cout << "You have won.";
            cout << "\n found at " << result;
	}
        else
        {
            cout<<"Nothing Found"<<endl;
	}
   
                   
    return 0;
}
int linearSearch(int array[], int size,int numFinder)
{
  
	for(int i=0; i<size;i++){
		if(numFinder==array[i]){
        return i;
		}
	}
	return -1;

}
