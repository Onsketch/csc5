//Andrew Lalyre               CSC5           Chapter 2, p. 83, #17
//
/*****************************************************************
 *
 * Stock plus Commission
 *________________________________________________________________
 * This program will calculate the price of stocks being bought
 * as well as the commission price and price of stocks with
 * commission.
 *
 *________________________________________________________________
 *INPUT
 *  shares              : Variable that will hold the number of
 *                        stocks bought,
 *  price               : Variable that will hold cost of a stock
 *  commission          : Variable that will hold a percent value

 * OUTPUT
 *  total_stock         : price of stocks bought.
 *  whole_commission    : amount that has to be paid as commission
 *  amount paid         : final price that has to be paid
 *
 ******************************************************************/
#include <cstdlib>
#include <iostream>

using namespace std;


int main() {

    double shares, price, commission ;
    double total_stock, whole_commission, amount_paid;

           shares     = 600;     // Kathryn bought 600 shares of stock in a
                                 // company.
           price      = 21.77;   // price Kathryn paid for her shares of stock
           commission = .02;     // the percent of commission taken

           // formula to get the price of stocks bought.
           total_stock = price*shares;

           // formula to get the price of commission fees.
           whole_commission = total_stock*commission;

           // formula to get the total cost.
           amount_paid = total_stock+whole_commission;

           cout << "Kathryn  spent a total of $" << total_stock <<" in stocks.\n";
           cout << "Kathryn will all so spend $" << whole_commission << " on ";
           cout << "commission fees.\n";
           cout << "In total Kathryn will spend $" << amount_paid <<endl;

    return 0;
}
