//Andrew Lalyre                CSC5           Chapter 2, p. 81, #1
//
/*****************************************************************
 *
 * Sum of Two Numbers
 *________________________________________________________________
 * This program will do a very basic mathematical addition problem
 *
 *________________________________________________________________
 *INPUT
 *  num1           // will hold the number 62
 *  num2           // will hold the number 99
 *
 * OUTPUT
 *  total          // will hold the added values of num1 and num2
 *
 ******************************************************************/

#include <iostream>
using namespace std;

int main() {
	int total, num1,num2;

        // number that will be used in a mathematical problem
        num1 = 62;

        // number that will be used in a mathematical problem
        num2 = 99;

        // will get a number by adding num1 and num2
        total = num1+num1;

        cout << num1 <<" + " << num2 << " = " << total << endl;
	return 0;
}
