//Andrew Lalyre               CSC5           Chapter 2, p. 83, #16
//
/*****************************************************************
 *
 * Diamond Pattern
 *________________________________________________________________
 * This program will will output a a diamond pattern using
 * for-loops nested inside a for-loop
 *
 *________________________________________________________________
 *INPUT
 * i                 // used as a counter
 * j                 // used as a counter
 * k                 // used as a counter
 *
 * OUTPUT
 *  a Asterisk diamond pattern
 *
 ******************************************************************/

#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
    for (int i = 1; i <= 4; i++)
    {
          for (int j = 0; j < (4 - i); j++)
                cout << " ";
          for (int j = 1; j <= i; j++)
                cout << "*";
          for (int k = 1; k < i; k++)
                cout << "*";
          cout << "\n";
    }

    for (int i = 4 - 1; i >= 1; i--)
    {
          for (int j = 0; j < (4 - i); j++)
                cout << " ";
          for (int j = 1; j <= i; j++)
                cout << "*";
          for (int k = 1; k < i; k++)
                cout << "*";
          cout << "\n";
    }


    return 0;
}
