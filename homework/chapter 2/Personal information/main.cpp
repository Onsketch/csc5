//Andrew Lalyre       CSC5           Chapter 2, p. 83, #14
//
/*****************************************************************
 *
 * COMPUTE Personal Information
 *________________________________________________________________
 * This program computes personal information then displays it to
 * the screen
 *
 *________________________________________________________________
 * INPUT
 *   name     : This stores a name in a variable
 *   address  : This stores an address in a variable
 *   city     : This stores a city in variable.
 *   st       : This stores intitnals of a state in variable.
 *   zip      : this stores a sequence of numbers.
 *   num      : This stores a line of numbers.
 *   major    : This stores a string of words in a variables.
 *
 * OUTPUT
 *  Andrew Lalyre
 *  420 Paper St.
 *  Wilmington
 *  DE
 * 19886
 * 323-391-5371
 * computer programming
 *
 ****************************************************************/
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
int main(){

    // variable that will hold a name
    char name[30] = "Andrew Lalyre";

    // variable that will hold an address
    char  address[30] = "420 Paper St.";

    // variable that will hold a name of a city
    char city [30]= "Wilmington";

    // variable that will hold initial of state
    char st[30] = "DE";

    // variable that will hold zip code
    int zip = 19886;

    // variable that will hold a phone number
    char num[30] = "323-391-5371";

    // variable that will hold a name of major
    char major[30] = "Computer Programming";

    cout << name << "\n" << address << " "<< city << " "
            << st << " " << zip << "\n"<<num << "\n" <<major;

	return 0;
	}
