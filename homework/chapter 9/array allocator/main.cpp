//Andrew Lalyre                      CSC5                  Chapter 8, p. 488, #8
//
/*******************************************************************************
 *
 * Search comparison 
 *______________________________________________________________________________
 * This program will allow the user to enter an account number and the program
 * will check if the numbered entered exist. 
 *
 *______________________________________________________________________________
 * INPUT
 *   array
 *   accountNumber
 *   
 * OUTPUT
 *   result
 * 
 ******************************************************************************/


#include <cstdlib>
#include <iostream>

using namespace std;


int main() {

    
    int amount = 10;
    int *amountPtr;
    amountPtr = &amount;
    
    cout << amount << endl;
    cout << sizeof(amount) << endl;
    cout << &amount << endl;
    cout << amountPtr << endl;
    cout << *amountPtr << endl;
    return 0;
}

