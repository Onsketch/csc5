//Andrew Lalyre                      CSC5                  Chapter 8, p. 488, #8
//
/*******************************************************************************
 *
 * Search comparison 
 *______________________________________________________________________________
 * This program will allow the user to enter an account number and the program
 * will check if the numbered entered exist. 
 *
 *______________________________________________________________________________
 * INPUT
 *   array
 *   accountNumber
 *   
 * OUTPUT
 *   result
 * 
 ******************************************************************************/


#include <cstdlib>
#include <iostream>

using namespace std;
int doSomething(int *, int *);

int main() {

    
    int a ;
    int b ;
    int sum;
    doSomething(&a,&b);
    cout << sum;
    
    return 0;
}

int doSomething(int *x, int *y)
{
    int temp = x;
    x = (y * 10);
    y = (temp * 10);
    return x + y;
}
