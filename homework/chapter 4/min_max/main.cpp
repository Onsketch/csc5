//Andrew Lalyre              CSC5            Chapter 4, p. 220, #1
//
/*****************************************************************
 *
 * Minimum/Maximum
 *________________________________________________________________
 * This program will display an mathematical problem involving 
 * addition
 *
 *________________________________________________________________
 *INPUT
 *  num1           // will get a number from the user
 *  num2           // will get a number from the user
 *  
 *
 * OUTPUT
 *  num1           // will output a number
 *  num2           // will output a number
 *
 ******************************************************************/
#include <iostream>
#include <string>
using namespace std;
int main()
{

    int num1;   // Input - will get a number from the user
    int num2;   // Input - will get a number from the user
    
    // will prompt user for two numbers
    cout << "Please enter two numbers: ";
    cin >> num1 >> num2;
    
    // lines 35 - 42 checks whick number is bigger
    if (num1 > num2)
    {
        cout << num1 << " is greather than " << num2;
    }
    else 
    {
        cout << num2 << " is greather than " << num1;
    }
    return 0;
}

