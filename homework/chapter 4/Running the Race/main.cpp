//Andrew Lalyre              CSC5           Chapter 4, p. 223, #14
//
/*****************************************************************
 *
 * Running the Race
 *________________________________________________________________
 * this program will get 3 names for runners and their fastest
 * time in a 100m race, then display who was the fastest
 *________________________________________________________________
 *INPUT
 *  first_runner   // name of the first runner
 *  second_runner  // name of the second runner
 *  third_runner   // name of the third runner
 *  
 * OUTPUT
 *  first_runner_speed   // time for the first runner
 *  second_runner_speed  // time for the second runner
 *  third_runner_speed   // time for the third runner
 *
 ******************************************************************/
#include <iostream>
#include <string>
using namespace std;
int main()
{
    string first_runner;        // Input - name of the first runner
    string second_runner;       // Input - name of the second runner
    string third_runner;        // Input - name of the third runner
    double first_runner_speed;  // OUTPUT - time for the first runner
    double second_runner_speed; // OUTPUT - time for the second runner
    double third_runner_speed;  // OUTPUT - time for the third runner
    
    cout << "Enter 3 runners that comepete in 100m dash\n";
    cout << "First runner: ";
    getline (cin,first_runner);
    cout << "Second runner: ";
    getline (cin,second_runner);
    cout << "Third runner: ";
    getline (cin,third_runner);
    cout << endl;
    
    cout << "Enter time for First runner: ";
    cin >> first_runner_speed;
    while (first_runner_speed < 0)
    {
        cout << "you have enterd an invaild number try agian: ";
                cin >> first_runner_speed;
    }
    cout << "Enter time for Second runner: ";
    cin >> second_runner_speed;
    while (second_runner_speed < 0)
    {
        cout << "you have enterd an invaild number try agian: ";
                cin >> second_runner_speed;
    }
    cout << "Enter time for Third runner: ";
    cin >> third_runner_speed;
    while (third_runner_speed < 0)
    {
        cout << "you have enterd an invaild number try agian: ";
                cin >> third_runner_speed;
    }        
    
    if (first_runner_speed >= second_runner_speed && third_runner_speed)
    {
        cout << first_runner_speed << " was the fastest in second is ";
                if(second_runner_speed >= third_runner_speed)
                {
                 cout << second_runner_speed << " followed by ";
                 cout << third_runner_speed;   
                }
                else 
                {
                    cout << third_runner_speed << " folloed by ";
                    cout << second_runner_speed;
                }
                    
    }
    if (second_runner_speed >=  first_runner_speed&& third_runner_speed)
    {
        cout << second_runner_speed << " was the fastest in second is ";
                if(first_runner_speed >= third_runner_speed)
                {
                 cout << first_runner_speed << " followed by ";
                 cout << third_runner_speed;   
                }
                else 
                {
                    cout << third_runner_speed << " folloed by ";
                    cout << first_runner_speed;
                }
                    
    }
    if (third_runner_speed >= second_runner_speed &&first_runner_speed )
    {
        cout << third_runner_speed << " was the fastest in second is ";
                if(second_runner_speed >= first_runner_speed)
                {
                 cout << second_runner_speed << " followed by ";
                 cout << first_runner_speed;   
                }
                else 
                {
                    cout << first_runner_speed << " folloed by ";
                    cout << second_runner_speed;
                }
                    
    }
    return 0;
}

