//Andrew Lalyre              CSC5           Chapter 4, p. 226, #24
//
/*****************************************************************
 *
 * Internet Service Provider pt.2
 *________________________________________________________________
 * This program will calucalte how much someone will spend on 3
 * diffent packages
 *________________________________________________________________
 *INPUT
 *  choice;       // used to switch between package
 *  hours;        // used as the hours that are provided
 *  over_hour;    // used to get the hours past what was alloted 
 *  base;         // has a base price for package
 *  over_base;    // cost of hours that excended alloted
 *
 * OUTPUT
 *  total;        // used as a holder for the number in base
 *  over_total;   // used as holder for hours muliply by base
 *  grand_total;  // used as a holder for base multply by over_
 *                // total
 *
 ******************************************************************/
#include <iostream>
#include <iomanip>
using namespace std;

int main() {

   int choice;          // Input - used to switch between package
   double hours;        // Input - used as the hours that are provided
   double over_hour;    // Input - used to get the hours past what was alloted 
   double base;         // Input - has a base price for package
   double over_base;    // Input - cost of hours that excended alloted
   double total;        // Output - used as a holder for the number in base
   double over_total;   // Output - used as holder for hours muliply by base
   double grand_total;  // Output - used as a holder for base multply by over_
 *                      // total
   
  cout << "Welcome to Internet Service. \nPress 1 to read about package A"
          "starting at $9.95.\npress 2 to read about package B priced at $14.95"
          " and \npress 3 to hear about package C at $19.95 " << endl;
    cin >> choice;
    cout << endl;

switch(choice)
{
      	case 1: 
            cout << "Details about package A are for $9.95 per month 10 hours\n"
                    "of access are provided. Additional hours are $2.00 "
                    "per hour." << endl;
            base = 9.95;
            over_base = 2.00;
            
            cout << "To calculate your bill please enter the number of hours "
                    "you plan to use: ";
                cin >> hours;
            
            if (hours <= 10)
            {
                total = base;
                
               cout << fixed << showpoint << setprecision(2);
               cout << "Your total for the month is $" << total << endl;
               
               base = 14.95;
               cout << "If you bought Plan B you would pay $"<< base << endl;
               
               base = 19.95;
               cout << "If you bought Plan C you would pay $"<< base << endl;
            }
            else if (hours > 10 && hours <= 744)
            {
                over_hour = hours - 10;
                base; 
                over_total = over_hour * over_base;
                grand_total = base+over_total;
                
               cout << fixed << showpoint << setprecision(2);
               cout << "Your total for the month is $" << grand_total << endl;
               
               base = 14.95;
               over_base = 1.00;
               over_hour = hours - 20;
               over_total = over_hour * over_base;
               grand_total = base+over_total;
               cout << fixed << showpoint << setprecision(2);
               cout << "If you bought Plan B you would pay $"<< grand_total << endl;
               
               base = 19.95;
               cout << "If you bought Plan C you would pay $"<< base << endl;
            }
            else
            {
               cout << "You have entered more hours than there are in a month.";   
            }              
            
	break;
	
	case 2: 
            cout << "Details about package B are for $14.95 per month 20 hours\n"
                    "of access are provided. Additional hours are $1.00 "
                    "per hour."<< endl;
            base = 14.95;
            over_base = 1.00;
            
            cout << "To calculate your bill please enter the number of hours "
                    "you plan to use: ";
                cin >> hours;
            
            if (hours <= 20)
            {
                total = base;                
               cout << fixed << showpoint << setprecision(2);
               cout << "Your total for the month is $" << total << endl;
               if (hours <= 10)
               {
                   base = 9.95;
                   cout << fixed << showpoint << setprecision(2);
                   cout << "If you bought Plan A you would pay $"<< base << endl;
               }
               else
               {
                   base = 9.95;
                   over_hour = hours - 10;
                   over_total = over_hour * over_base;
                   grand_total = base+over_total;
               cout << fixed << showpoint << setprecision(2);
               cout << "If you bought Plan A you would pay $"<< grand_total << endl;
               }
               base = 19.95;
               cout << "If you bought Plan C you would pay $"<< base << endl;
            }
            else if (hours > 20 && hours <= 744)
            {
                over_hour = hours - 20;
                over_total = over_hour * over_base;
                grand_total = base+over_total;
               
               cout << fixed << showpoint << setprecision(2);
               cout << "Your total for the month is $" << grand_total << endl;
               
                   base = 9.95;
                   over_hour = hours - 10;
                   over_total = over_hour * over_base;
                   grand_total = base+over_total;
               cout << fixed << showpoint << setprecision(2);
               cout << "If you bought Plan A you would pay $"<< grand_total << endl;
               
               base = 19.95;
               cout << "If you bought Plan C you would pay $"<< base << endl;
                
                
            }
            else
            {
               cout << "You have entered more hours than there are in a month.";   
            }
                            
	break;
	
	case 3: 
               cout << "Details about package C are for $19.95 per month "
                    "unlimited access is provided" <<endl;
               cout << "To calculate your bill please enter the number of hours "
                    "you plan to use: ";
                cin >> hours;
            base = 19.95;
            total = base;
            
            if (hours <= 744)
            {
               cout << fixed << showpoint << setprecision(2);
               cout << "Your total for the month is $" << total << endl;
               
               if (hours <= 10)
               {
                   base = 9.95;
                   cout << fixed << showpoint << setprecision(2);
                   cout << "If you bought Plan A you would pay $"<< base << endl;
               }
               else
               {
                   base = 9.95;
                   over_base = 2.00;
                   over_hour = hours - 10;
                   over_total = over_hour * over_base;
                   grand_total = base+over_total;
               cout << fixed << showpoint << setprecision(2);
               cout << "If you bought Plan A you would pay $"<< grand_total << endl;
               }
               
                base = 14.95;
                over_base = 1.00;
                over_hour = hours - 20;
                over_total = over_hour * over_base;
                grand_total = base+over_total;
               
               cout << fixed << showpoint << setprecision(2);
               cout << "If you bought Plan B you would pay $"<< grand_total << endl;
            }
            else 
            {
               cout << "You have entered more hours than there are in a month."; 
            }
            
	break;
	
	default: 
            cout << "You have chosen not to obey the rules. For this you must "
                    "punished.\nA Ninja shall oversee this matter.";
}


	return 0;
}