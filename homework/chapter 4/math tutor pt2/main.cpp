//Andrew Lalyre              CSC5           Chapter 2, p. 146, #15
//
/*****************************************************************
 *
 * Math Tutor
 *________________________________________________________________
 * This program will display an mathematical problem involving 
 * addition
 *
 *________________________________________________________________
 *INPUT
 *  num1           // will hold a random number from 0 - 999
 *  num2           // will hold a random number from 0 - 999
 *  userInput      // user input to exist program
 *
 * OUTPUT
 *  answer          // will hold the added values of num1 and num2
 *  usersAnswer     // user's response to the math problem
 *
 ******************************************************************/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

int main() {
    string userInput;
    
    // a while loop is being used to have the user solve as many 
    // many math problems as he/she desires.
    while ( userInput != "s" )
    {
    // srand(time(0) is used to get a random number
    srand(time(0));
    
    int num1;             // will hold a number value
    int num2;             // will hold a number value
    int answer;     // will hold the added value of num1 and num 2
    int usersAnswer;    // will get a number value from the user
    
    // gets random number from 0 - 999
    num1 = rand() % 1000;
    // gets random number from 0 - 999
    num2 = rand() % 1000;
    // adds the values from num1 and num2
    answer = num1 + num2;
    
    // lines 51 to 55 will display a math problem
    cout << setw(4) << num1 << endl;
    cout << "+" << setw(3) << num2 << endl;
    cout << "____" << endl;
    cin >> usersAnswer;
    cout << endl;
    
    // lines 59 to 68 is an if / else statement that will check
    // whether or not the answer for the math problem is correct  
    if (usersAnswer == answer)
    {
        cout << "Good Job" << endl; 
        cout << "The answer is: " << answer;
    }
    else 
    {
        cout << "Incorrect" << endl;
        cout << "The answer is: " << answer;
    }
    
    // lines 71 to 74 checks if the conditions of the loop as
    // been satisfied or not.
    cout << endl;
    cout << "\nPress a to play again or press s to stop: ";
    cin >> userInput;
    cout << endl;
    
    }
    
    return 0;
}

