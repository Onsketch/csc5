//Andrew Lalyre                      CSC5                  Chapter 2, p. 143, #5
//
/*******************************************************************************
 *
 * Box Office
 *______________________________________________________________________________
 * This program will calculate the gross and net profit for a movie at the box
 * office.
 *
 *______________________________________________________________________________
 * INPUT
 *   movieName            // gets a movie tile 
 *   adultTicketsSold     // number used to repesent how many adult tickets sold
 *   childTicketsSold     // number used to repesent how many child tickets sold
 *   
 * OUTPUT
 *   grossIncome          // how much money the movie theater took in
 *   netIncome            // how much money the theater made
 *   amountPaid           // how much the movie theater has to pay
 *
 * CONST
 *   ADULTADMISSIONPRICE  // the price for an adult ticket is 6.00
 *   CHILDADMISSIONPRICE  // the price for a child ticket is 3.00
 *   profitPercent        // commission percent is 0.20
 * 
 ******************************************************************************/

#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

int main() {
    const double ADULTADMISSIONPRICE = 6.00;
    const double CHILDADMISSIONPRICE = 3.00;
    const double profitPercent = 0.20;
    
    string movieName;      // Input- gets a movie tile 
    int adultTicketsSold;  // Input- number used to repesent how many adult
                           //        tickets sold
    int childTicketsSold;  // Input- number used to repesent how many acild 
                           //        tickets sold
    double grossIncome;    // OutPut- how much money the movie theater took in
    double netIncome;      // OutPut- how much money the theater made
    double amountPaid;     // OutPut- how much the movie theater has to pay
         
    // lines 48 - 53 will get the info such as the movie title, and tickets sold
    cout << "Please enter name of movie: ";
    getline (cin,movieName);
    cout << "Enter how many Adult tickets were sold: ";
    cin >> adultTicketsSold;
    cout << "Please enter how many children tickets were sold: ";
    cin >> childTicketsSold;
    
    // to figure out the groos income you will mulitply adult tickets sold
    // and adult admission then add it to the results of child ticket muiltpied
    // by child admission
    grossIncome = (adultTicketsSold*ADULTADMISSIONPRICE)+(childTicketsSold* 
            CHILDADMISSIONPRICE);
    
    // to get the net income you mulitply gross income by the profit percent        
    netIncome = grossIncome * profitPercent;
    
    // to get the amount the theater has to pay you subtact the netIncome by
    // gross income
    amountPaid = grossIncome - netIncome;
    
    
    // lines 72 - 82 display the information of the movie , how many tickets 
    // were sold as well as how much the theater made and how much they owe to
    // the distrubor
    cout << endl;
    cout << fixed << showpoint << setprecision(2);
    cout << "Movie Name: " << setw(35) <<'"' << movieName << '"' << endl;
    cout << "Adult Tickets Sold: " << setw(31) << adultTicketsSold << endl;
    cout << "Child Tickets Sold: " << setw(31) << childTicketsSold << endl;
    cout << "Gross Box Office Profit: " << setw(21) << "$" 
            <<setw(8) << grossIncome << endl;
    cout << "Net Box Office Profit: " << setw(23) << "$" 
            <<setw(8) << netIncome << endl;
    cout << "Amount Paid to Distributor: " << setw(18) << "$" 
            <<setw(8) << amountPaid << endl;
    
    
    return 0;
}