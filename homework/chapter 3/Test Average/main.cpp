//Andrew Lalyre                       CSC5                 Chapter 2, p. 143, #3
//
/*******************************************************************************
 *
 * Test Average
 *______________________________________________________________________________
 * This program will take in five test score numbers and display the average
 *
 *______________________________________________________________________________
 *INPUT
 *  test_score1     // holds the first test score
 *  test_score2     // holds the second test score
 *  test_score3     // holds the third test score
 *  test_score4     // holds the fourth test score
 *  test_score5     // holds the fifth test score
 * 
 * OUTPUT
 *  average         // will display the average of the test scores
 *
 ******************************************************************************/
 
#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;
 
int main() {
 
    float test_score1;   // Input - holds the first test score.
    float test_score2;   // Input - holds the second test score.
    float test_score3;   // Input - holds the third test score.
    float test_score4;   // Input - holds the fourth test score.
    float test_score5;   // Input - holds the fifth test score.
    float average;       // Output - will display the average of the scores 
                         // entered
 
    // lines 39-49 will be used to gether the data to find the average of the
    // scores
    cout << "Welcome to the 5 test score average calculator\n";
    cout << "Please enter the first score: ";
    cin >> test_score1;
    cout << "Please enter the second score: ";
    cin >> test_score2;
    cout << "Please enter the third score: ";
    cin >> test_score3;
    cout << "Please enter the fourth score: ";
    cin >> test_score4;
    cout << "Please enter the fifth score: ";
    cin >> test_score5;
 
    // to get the average of the scores you need to add up all the values and 
    // divide by 5
    average = (test_score1+test_score2+test_score3+test_score4+test_score5)/5;
 
    // line 56 is used to get a single percision on the outputed values.
    cout << fixed << showpoint << setprecision(1);
 
    // line 59 is going to display a message along with the results of line 53
    cout << "the average is " << average;
 
    return 0;
}