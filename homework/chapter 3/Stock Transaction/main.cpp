//Andrew Lalyre              CSC5           Chapter 2, p. 147, #21
//
/*****************************************************************
 *
 * Stock Transaction
 *________________________________________________________________
 * This program will calculate how much Joe spent on his shares 
 * of stocks. How much he spent on commission fees as well as how 
 * much he made after selling his stocks and how much he paid on 
 * commission fees for selling his stocks,
 *
 *________________________________________________________________
 *INPUT
 *  stocks          // the number of stocks bought      
 *  buyingPrice     // the price of buying each stock 
 *  commissionFee   // percent for commission
 *  sellingPrice    // the price of selling each stock
 *
 * OUTPUT
 *  costOfStocks    // amount paid for all the stocks
 *  paidCommission  // amount that was paid on commission fees
 *  sellOfStocks    // amount that the stocks sold for
 *  sellCommission  // amount that was paid on commission fees
 *  totalCommission // total spent on commission fees
 *  profit          // if any profit was made
 *
 ******************************************************************/

#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

int main() {

    double stocks;           // Input the number of stocks bought 
    double buyingPrice;      // Input the price of buying each stock
    double commissionFee;    // Input percent for commission
    double sellingPrice;     // Input the price of selling each 
                             // stock
    double costOfStocks;     // Output amount paid for all the 
                             // stocks
    double paidCommission;   // Output amount that was paid on 
                             // commission fees
    double sellOfStocks;     // Output amount that was paid on 
                             // commission fees
    double sellCommission;   // Output total spent on commission 
                             // fees
    double totalCommission;  // Output total spent on commission 
                             // fees
    double profit;           // Output if any profit was made
    
    
    // lines 54 - 57 are variables that are being assigned a value 
    stocks = 1000;
    buyingPrice = 32.87;
    commissionFee = .02;
    sellingPrice = 33.92;
    
    // to figure out how much the stocks cost you will need to take 
    // the amount of stocks you are buying and multiply it by 
    // what the buying cost is per stock
    costOfStocks = stocks * buyingPrice;
    // to figure out how much you are paying in commission fess 
    // you take the cost if the stocks and multiply it by what 
    // the commission fee is
    paidCommission = (costOfStocks * commissionFee);
    // to get how much Joe made by selling his stock you need to 
    // take the amount of stocks and multiply it by the selling cost
    sellOfStocks = stocks * sellingPrice;
    // to figure out how much you are paying in commission fess 
    // you take the cost if the stocks and multiply it by what 
    // the commission fee is 
    sellCommission = (sellOfStocks * commissionFee);
    // to get how much was paid in both transaction you need to
    // add the amount from the first commission fee to the second
    // fee
    totalCommission = paidCommission + sellCommission;
    // to get the profit of what was made you take the cost of 
    // stocks and subtract it by the sell of the stocks then 
    // you subtract that from the total amount of commission 
    // that was taken out
    profit = (sellOfStocks - costOfStocks) - totalCommission;
    
    cout << fixed << showpoint << setprecision(2);
    cout << "Joe thought about buying stock in Acme Software, Inc." << endl;
    cout << "He went ahead and bought " << stocks << " at $" << buyingPrice;
    cout << " per share. "<< endl;
    cout << "Joe paid a total of $" << costOfStocks << " for the " << stocks;
    cout << " shares of \nstock plus $" << paidCommission ;
    cout << " on commission fees";
    cout << "\nAfter the price of the stocks had gone up to $" << sellingPrice;
    cout << " \nJoe decided to sell. He received $" << sellOfStocks << " For ";
    cout << "selling \nand had to pay $" << sellCommission;
    cout << " in commission fees";
    cout << "\nAfter selling the stocks and paying commission fees";
    cout << "\nJoe lost a total of $" << profit << " in this deal";
    
    return 0;
}
