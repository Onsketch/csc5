// Andrew Lalyre                   CSC5                    Chapter 2, p.148, #22
//
/*******************************************************************************
*
* Word Game
*_______________________________________________________________________________
*This program computes the distance a car can travel on one
*tank of gas based on gas tank capacity and average
*mile-per-gallon (MPG) rating for city and highway driving.
*
*Computation is based on the formula:
*Distance = Number of Gallons x Average Miles per Gallon
*_______________________________________________________________________________
*INPUT
*  name        :
*  age         :
*  city        :
*  college     :
*  profession  :
*  animal      :
*  pet_Name    :
*
*OUTPUT
*  word game message
*
 ******************************************************************************/

#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
int main()

{
	string name;
	int     age;
	string city;
	string college;
	string profession;
	string animal;
	string pet_Name;

	cout << "Please enter a name: ";
	getline(cin,name);

	cout << "Enter an age: ";
	cin >> age;

	cout << "Enter a City name: ";
	getline(cin,city);
	cin.ignore(1000,'\n');

	cout << "Please enter a University name: ";
	getline(cin,college);

	cout << "Enter a Profession: ";
	getline(cin,profession);

	cout << "Enter an Animal: ";
	getline(cin,animal);

	cout << "Enter a name for a pet: ";
	getline(cin,pet_Name);

	cout << endl;
	cout << "There once was a person names " << name;
	cout << ", who lived in " << city << ". " << endl;
	cout << "At the age of " << age << ", "  << name;
	cout << " went to college at " << college << ". "<< endl;
	cout << name << " graduated and went to work as a ";
	cout << profession << ". " << endl;
	cout << "Then, " << name << "adopted ";
	cout << "a(n) " << animal << " named " << pet_Name;
	cout << ". " ;
	cout <<	"They both lived happily ever after!";

	return 0;
}
