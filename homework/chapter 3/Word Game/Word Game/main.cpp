// Andrew Lalyre                   CSC5                    Chapter 2, p.148, #22
//
/*******************************************************************************
*
* Word Game
*_______________________________________________________________________________
* This program will gather different string data types to output a funny story
*_______________________________________________________________________________
*INPUT
*  name        : name of someone
*  age         : an age 
*  city        : name of a city
*  college     : name of a college
*  profession  : a job title
*  animal      : an animal 
*  pet_Name    : name of a pet 
*
*OUTPUT
*  word game message
*
 ******************************************************************************/

#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
int main()

{
	string name;        // Input a name
	int     age;        // Input a number for an age
	string city;        // Input a name for a city
	string college;     // Input the name of a college
	string profession;  // Input a job title
	string animal;      // Input an animal
	string pet_Name;    // Input a name for a bit

    // lines 39 - 59 are used to gather information necessary for the word game
	cout << "Please enter a name: ";
	getline(cin,name);

	cout << "Enter an age: ";
	cin >> age;
        cin.ignore(1000,'\n');

	cout << "Enter a City name: ";
	getline(cin,city);
	
	cout << "Please enter a University name: ";
	getline(cin,college);

	cout << "Enter a Profession: ";
	getline(cin,profession);

	cout << "Enter an Animal: ";
	getline(cin,animal);

	cout << "Enter a name for a pet: ";
	getline(cin,pet_Name);


    // lines 63 - 73 is the word game that will dispay the story along with 
    // the data the user has inputed.
	cout << endl;
	cout << "There once was a person named " << name;
	cout << ", who lived in " << city << ". " << endl;
	cout << "At the age of " << age << ", "  << name;
	cout << " went to college at " << college << ". "<< endl;
	cout << name << " graduated and went to work as a ";
	cout << profession << ". " << endl;
	cout << "Then, " << name << " adopted ";
	cout << "a(n) " << animal << " named " << pet_Name;
	cout << ". " << endl;
	cout <<	"They both lived happily ever after!";

	return 0;
}