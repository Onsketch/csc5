//Andrew Lalyre                CSC5           Chapter 2, p. 81, #1
//
/*****************************************************************
 *
 * Sum of Two Numbers
 *________________________________________________________________
 * This program will do a very basic mathematical addition problem
 *
 *________________________________________________________________
 *INPUT
 *  num1           // will hold the number 62
 *  num2           // will hold the number 99
 *
 * OUTPUT
 *  total          // will hold the added values of num1 and num2
 *
 ******************************************************************/

#include <iostream>
#include <cstdlib>
using namespace std;

int main() {

    return 0;
}

